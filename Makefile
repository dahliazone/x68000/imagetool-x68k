TARGET := imagetool-x68k

SRC_DIRS := ./src

SRCS := $(shell find $(SRC_DIRS) -name "*.c")
OBJS := $(SRCS:%=%.o)
DEPS := $(OBJS:.o=.d)

LIB_Z_DIR := ./lib/zlib-1.2.13
LIB_PNG_DIR := ./lib/libpng-1.6.39
LIB_Z := $(LIB_Z_DIR)/libz.a
LIB_PNG := $(LIB_PNG_DIR)/.libs/libpng16.a

INC_DIRS := $(shell find $(SRC_DIRS) -type d) $(LIB_Z_DIR) $(LIB_PNG_DIR)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CFLAGS += -std=c99 -Wall -Wextra -Wshadow -Wdouble-promotion -Wundef -fno-common -Wconversion -Wformat-truncation
# -Wformat=2
LDFLAGS += "$(LIB_PNG)" "$(LIB_Z)" -lm


all: libs $(TARGET)

.PHONY: help
help:
	@echo "Available make targets:"
	@echo
	@echo "all               Build dependency libraries and tool"
	@echo
	@echo "$(TARGET)\t  Build $(TARGET)"
	@echo "clean             Clean build"
	@echo
	@echo "libs             Build all libraries"
	@echo "libs-clean       Clean library builds"

$(TARGET): $(OBJS)
	$(CC) $(OBJS) -o $@ $(LDFLAGS)

%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(INC_FLAGS) $(CFLAGS) -c $< -o $@

test: $(TARGET)

.PHONY: clean
clean:
	$(RM) -r $(TARGET) src/*.o *~ src/*~

libscheck:
	@stat  lib/libpng-1.6.39/configure lib/zlib-1.2.13/configure >/dev/null || (echo Please uncompress libs and/or initialize submodules! && false)

$(LIB_Z):
	(cd lib/zlib-1.2.13 && ./configure && make)

$(LIB_PNG):
	(cd lib/libpng-1.6.39 && LDFLAGS="-L../zlib-1.2.13" CPPFLAGS="-I../zlib-1.2.13" ./configure && make)

libs: libscheck lib/zlib-1.2.13/libz.a lib/libpng-1.6.39/.libs/libpng16.a

libs-clean:
	make -C lib/libpng-1.6.39 clean
	make -C lib/zlib-1.2.13 clean

-include $(DEPS)

MKDIR_P ?= mkdir -p
