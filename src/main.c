#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <png.h>

const char COLOR_GRAY[] = "gray";
const char COLOR_PALETTE[] = "palette";
const char COLOR_RGB[] = "rgb";
const char COLOR_RGB_ALPHA[] = "rgba";
const char COLOR_GRAY_ALPHA[] = "gray+alpha";
const char INTERLACE_NONE[] = "none";
const char INTERLACE_ADAM7[] = "adam7";
const char EMPTY[] = "";


/**
 * @returns Non-zero (true) if given file is PNG.
 */
#define PNG_BYTES_TO_CHECK 4
int check_if_png_fp(FILE* fp)
{
	fseek(fp, 0, SEEK_SET);

	png_byte buf[PNG_BYTES_TO_CHECK];
	if (fread(buf, 1, PNG_BYTES_TO_CHECK, fp) != PNG_BYTES_TO_CHECK) {
		return 0;
	}

	return !png_sig_cmp(buf, 0, PNG_BYTES_TO_CHECK);
}


/**
 * @returns -1 if file is not found, 0 if file is not PNG or 1 is if it is.
 */
int check_if_png(const char* file_name)
{
	FILE* fp = fopen(file_name, "rb");
	if (fp == NULL) {
		return -1;
	}

 	int return_value = check_if_png_fp(fp);
	fclose(fp);

	return return_value == 0 ? 0 : 1;
}


int print_error(int error_code, const char* error_msg, ...)
{
	va_list args;
	va_start(args, error_msg);
	fprintf(stderr, error_msg, args);
	va_end(args);
	return error_code;
}


const char* get_color_type_str(int color_type)
{
	switch (color_type) {
		case PNG_COLOR_TYPE_GRAY:
			return COLOR_GRAY;
		case PNG_COLOR_TYPE_PALETTE:
			return COLOR_PALETTE;
		case PNG_COLOR_TYPE_RGB:
			return COLOR_RGB;
		case PNG_COLOR_TYPE_RGB_ALPHA:
			return COLOR_RGB_ALPHA;
		case PNG_COLOR_TYPE_GRAY_ALPHA:
			return COLOR_GRAY_ALPHA;
	}

	print_error(-1, "Invalid color type %i", color_type);
	return EMPTY;
}


const char* get_interlace_type_str(int interlace_type)
{
	switch (interlace_type) {
		case PNG_INTERLACE_ADAM7:
			return INTERLACE_ADAM7;
		case PNG_INTERLACE_NONE:
			return INTERLACE_NONE;
	}

	print_error(-1, "Invalid interlace type %i", interlace_type);
	return EMPTY;
}


int load_png(const char* file_name)
{
	int status = check_if_png(file_name);
	if (status == -1) {
		return print_error(-1, "File not found \"%s\"", file_name);
	}
	else if (status == 0) {
		return print_error(-1, "File not PNG \"%s\"", file_name);
	}

	FILE* fp = fopen(file_name, "rb");
	if (fp == NULL) {
		return print_error(-1, "Cannot open file \"%s\"", file_name);
	}

	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (png_ptr == NULL) {
		fclose(fp);
		return print_error(-1, "Fatal error at png_create_read_struct()");
	}

	png_infop info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL) {
		fclose(fp);
		png_destroy_read_struct(&png_ptr, NULL, NULL);
		return print_error(-1, "Fatal error at png_create_read_struct()");
	}

	if (setjmp(png_jmpbuf(png_ptr))) {
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		fclose(fp);
		return print_error(-1, "Fatal error in reading the file");
	}

	png_init_io(png_ptr, fp);

	png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

	// At this point you have read the entire image
	png_uint_32 width, height;
	int bit_depth, color_type, interlace_type;
	png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
		&interlace_type, NULL, NULL);

	printf("Read PNG image %u x %u, %i bpp, %s, interlace %s\n",
		width, height, bit_depth,
		get_color_type_str(color_type),
		get_interlace_type_str(interlace_type));

	png_bytepp rows = png_get_rows(png_ptr, info_ptr);
	png_uint_32 bytes_per_row = png_get_rowbytes(png_ptr, info_ptr);

	size_t bytes_per_pixel = 1;
	switch (color_type) {
		case PNG_COLOR_TYPE_GRAY:
			bytes_per_pixel = 1;
			break;
		case PNG_COLOR_TYPE_PALETTE:
			bytes_per_pixel = 1;
			break;
		case PNG_COLOR_TYPE_RGB:
			bytes_per_pixel = 3;
			break;
		case PNG_COLOR_TYPE_RGB_ALPHA:
			bytes_per_pixel = 4;
			break;
		case PNG_COLOR_TYPE_GRAY_ALPHA:
			bytes_per_pixel = 2;
			break;
	}

	if (width * bytes_per_pixel != bytes_per_row) {
		return print_error(-1, "Fatal error bytes per pixel does not match! width(%u) * bytes_per_pixel(%u) != bytes_per_row(%u)", width, bytes_per_pixel, bytes_per_row);
	}

	for (size_t i=0; i<height; i++) {
		for (size_t j=0; j<width * bytes_per_pixel; j += 3) {
			printf("%d %d %d ", rows[i][j], rows[i][j + 1], rows[i][j + 2]);
		}
		printf("\n");
	}

	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

	fclose(fp);

	return 0;
}





















// OLD SIMPLE --------------------------------------------------------------------------

int simple_read_write_png(const char* file_name, const char* file_name_out)
{
	int result = 0;
	png_image image;

	memset(&image, 0, sizeof image);
	image.version = PNG_IMAGE_VERSION;

	if (png_image_begin_read_from_file(&image, file_name)) {
		png_bytep buffer;

		image.format = PNG_FORMAT_RGBA;
		buffer = malloc(PNG_IMAGE_SIZE(image));
		if (buffer != NULL) {
			if (png_image_finish_read(&image, NULL/*background*/, buffer,
				0/*row_stride*/, NULL/*colormap for PNG_FORMAT_FLAG_COLORMAP */)) {
				if (png_image_write_to_file(&image, file_name_out,
					0/*convert_to_8bit*/, buffer, 0/*row_stride*/,
					NULL/*colormap*/)) {
					result = 0;
				}
				else {
					fprintf(stderr, "pngtopng: write %s: %s\n", file_name_out,
						image.message);
				}
			}
			else {
				fprintf(stderr, "pngtopng: read %s: %s\n", file_name,
					image.message);
			}
			free(buffer);
		}
		else {
			fprintf(stderr, "pngtopng: out of memory: %lu bytes\n",
				(unsigned long)PNG_IMAGE_SIZE(image));

			/* This is the only place where a 'free' is required; libpng does
			 * the cleanup on error and success, but in this case we couldn't
			 * complete the read because of running out of memory and so libpng
			 * has not got to the point where it can do cleanup.
			 */
			png_image_free(&image);
		}
	}
	else {
		/* Failed to read the first argument: */
		fprintf(stderr, "pngtopng: %s: %s\n", file_name, image.message);
	}

	return result;
}


// OLD SIMPLE END -------------------------------------------------------------------


int main()
{
	int result = 0;
	char file_name[60];

	strcpy(file_name, "temp/t101-rgb.png");
	result = simple_read_write_png(file_name, "temp/t1.png");
	if (result != 0) {
		printf("Failed to read file %s\n", file_name);
	}
	load_png(file_name);

	strcpy(file_name, "temp/t101-indexed16.png");
	result = simple_read_write_png(file_name, "temp/t2.png");
	if (result != 0) {
		printf("Failed to read file %s\n", file_name);
	}
	load_png(file_name);

	strcpy(file_name, "temp/kompot.png");
	result = simple_read_write_png(file_name, "temp/t3.png");
	if (result != 0) {
		printf("Failed to read file %s\n", file_name);
	}
	load_png(file_name);

	return 0;
}
